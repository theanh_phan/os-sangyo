<?php
/**
 * Created by PhpStorm.
 * User: UMEBIUS.COM
 * Date: 2016/09/09
 * Time: 23:41
 */


function putsCSVToFile($row_data, $file_path){
//    var_dump($row_data);

    $exists_flg = file_exists($file_path);
    $stream = fopen($file_path, 'a');
    if(!$exists_flg){
        $values = array_keys($row_data);
        mb_convert_variables('SJIS-win', 'UTF-8', $values);
        fputcsv($stream, $values);
    }

    $values = array_values($row_data);
    mb_convert_variables('SJIS-win', 'UTF-8', $values);
    fputcsv($stream, $values);
    fclose($stream);

    if(!$exists_flg){
        chmod($file_path, 0644);
    }

}


function putsCSVToFileWithNoMac($row_data, $file_path){
    var_dump($row_data);

    $stream = fopen($file_path, 'a');
    fwrite($stream, "\xef\xbb\xbf");
    $csv_header = array_keys($row_data);
    fputcsv($stream, $csv_header);
    fputcsv($stream, array_values($row_data));
    exit();
}