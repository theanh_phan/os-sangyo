<?php header("Content-Type:text/html;charset=utf-8"); ?>
<?php
ini_set( 'display_errors', 0 );
#######################################################################################
##
#  PHPメールプログラム
#　改造や改変は自己責任で行ってください。
#	
#  今のところ特に問題点はありませんが、不具合等がありましたら下記までご連絡ください。
#  MailAddress: support@kens-web.com
#  name: kenji.numata
#  HP: http://www.kens-web.com/
#  超重要！！サイトでチェックボックスを使用する場合のみですが。。。
#  チェックボックスを使用する場合はinputタグに記述するname属性の値を必ず配列の形にしてください。
#  例　name="当サイトをしったきっかけ[]"  として下さい。
#  nameの値の最後に[と]を付ける。じゃないと複数の値を取得できません！
##
#######################################################################################

mb_language('uni');
mb_internal_encoding('UTF-8');

//-----------------以下基本設定　必ず設定してください。--------------------------

//サイトのトップページのURL　※送信完了後に「トップページへ戻る」ボタンが表示されますので
$site_top = "/";

// このPHPファイルの名前 ※ファイル名を変更した場合は必ずここも変更してください。
$file_name ="mail.php";

// メールを受け取るメールアドレス(複数指定する場合は「,」で区切ってください)
$to = "ossangyo@ossangyo.co.jp";

// 送信完了後に自動的に指定のページに移動する(する=1, しない=0)
// 0にすると、送信終了画面が表示されます。
$jpage = 0;

// 送信完了後に表示するページ（上記で1を設定した場合のみ）
$next = "";

//--------- 以下は必要に応じて設定してください --------------

// 送信されるメールのタイトル（件名）
$sbj = "ホームページからお問い合わせ【オーエス産業株式会社】";

// 送信確認画面の表示(する=1, しない=0)
$chmail = 1;

// 差出人は、送信者のメールアドレスにする(する=1, しない=0)
// する場合は、メール入力欄のname属性の値を「Email」にしてください。
$from_add = 1;

// 差出人に送信内容確認メールを送る(送る=1, 送らない=0)
// 送る場合は、メール入力欄のname属性の値を「Email」にしてください。
//また差出人に送るメール本文の文頭に「○○様」と表示させるには名前入力欄のname属性を name="名前" としてください
$remail = 1;

// 差出人に送信確認メールを送る場合のメールのタイトル（上記で1を設定した場合のみ）
$resbj = "お問い合わせありがとうございました【オーエス産業株式会社】";


// 必須入力項目を設定する(する=1, しない=0)
$esse = 1;

/* 必須入力項目(入力フォームで指定したname属性の値を指定してください。（上記で1を設定した場合のみ）
日本語はシングルクォーテーションで囲んで下さい。複数指定する場合は「,」で区切ってください)*/
$eles = array('Email');


//--------------------- 基本設定ここまで -----------------------------------

// 以下の変更は知識のある方のみ自己責任でお願いします。

$sendmail = 0;
foreach($_POST as $key=>$val) {
  if($val == "submit") $sendmail = 1;
}

// 文字の置き換え
$string_from = "＼";
$string_to = "ー";

// 未入力項目のチェック
if($esse == 1) {
  $flag = 0;
  $length = count($eles) - 1;
  foreach($_POST as $key=>$val) {
    $key = strtr($key, $string_from, $string_to);
    if($val == "submit") ;
    else {
      for($i=0; $i<=$length; $i++) {
        if($key == $eles[$i] && empty($val)) {
          $errm .= "<FONT color=#ff0000>「".$key."」は必須入力項目です。</FONT><br>\n";
          $flag = 1;
        }
      }
    }
  }
  foreach($_POST as $key=>$val) {
    $key = strtr($key, $string_from, $string_to);
    for($i=0; $i<=$length; $i++) {
      if($key == $eles[$i]) {
        $eles[$i] = "check_ok";
      }
    }
  }
  for($i=0; $i<=$length; $i++) {
    if($eles[$i] != "check_ok") {
      $errm .= "<FONT color=#ff0000>「".$eles[$i]."」が未選択です。</FONT><br>\n";
      $eles[$i] = "check_ok";
      $flag = 1;
    }
  }
  if($flag == 1){
    htmlHeader();
?>
<!--- 未入力があった時の画面 --- 開始 ------------------- -->
入力エラー<br><br>
<?php echo $errm; ?>
<br><br>
<input type="button" value="前画面に戻る" onclick="history.back()">

<!--- 終了 - -->

<?php 
    htmlFooter();
    exit(0);
  }
}
// 届くメールのレイアウトの編集

$body="「".$sbj."」からメールが届きました\n\n";
$body.="■□■□■□■□■□■□■□■□■□■□■□■□■□\n\n";
$csv_row = array();
require_once ('csv_helper.php');
foreach($_POST as $key=>$val) {
  $key = strtr($key, $string_from, $string_to);
  
  //※numata追記　チェックボックス（配列）の場合は以下の処理で複数の値を取得するように変更した。　HTML側のname属性の値に[と]を追加する。
  $out = '';
  if(is_array($val)){
  foreach($val as $item){ 
  $out .= $item . '-'; 
  }
  if(substr($out,strlen($out) - 1,1) == '-') { 
  $out = substr($out, 0 ,strlen($out) - 1); 
  }
 }
    else {
        $out = $val;
}
  //チェックボックス（配列）追記ここまで
  if(get_magic_quotes_gpc()) { $out = stripslashes($out); }
  if($out == "submit") ;
  else $body.="【 ".$key." 】 ".$out."\n";

    if($out !== "submit") $csv_row[$key] = $out;
}
$body.="\n■□■□■□■□■□■□■□■□■□■□■□■□■□\n";
$body.="送信された日時：".date( "Y/m/d (D) H:i:s", time() )."\n";
$body.="送信者のIPアドレス：".$_SERVER["REMOTE_ADDR"]."\n";
$body.="送信者のホスト名：".getHostByAddr(getenv('REMOTE_ADDR'))."\n";
//--- 終了 --->


if($remail == 1) {
//--- 差出人への送信確認メールのレイアウト

if(isset($_POST['名前'])){ $rebody = "{$_POST['名前']} 様\n\n";}
$rebody.="お問い合わせありがとうございました。\n\n";
$rebody.="担当より追ってご返信させていただきますので、今しばらくお待ちくださいませ。\n\n";
$rebody.="送信内容は以下になります。\n\n";
$rebody.="■□■□■□■□■□■□■□■□■□■□■□■□■□\n\n";
foreach($_POST as $key=>$val) {
  $key = strtr($key, $string_from, $string_to);
  
  
  //※numata追記　チェックボックス（配列）の場合は以下の処理で複数の値を取得するように変更　HTML側のname属性の値に[と]を追加する。
  $out = '';
  if(is_array($val)){
  foreach($val as $item){ 
  $out .= $item . '-'; 
  }
  if(substr($out,strlen($out) - 1,1) == '-') { 
  $out = substr($out, 0 ,strlen($out) - 1); 
  }
 }
    else {
        $out = $val;
}
  //チェックボックス（配列）追記ここまで
  if(get_magic_quotes_gpc()) { $out = stripslashes($out); }
  if($out == "submit") ;
  
  else $rebody.="【 ".$key." 】 ".$out."\n";
}
$rebody.="\n■□■□■□■□■□■□■□■□■□■□■□■□■□\n\n";
$rebody.="送信日時：".date( "Y/m/d (D) H:i:s", time() )."\n";
$reto = $_POST['Email'];
//$rebody=mb_convert_encoding($rebody,"JIS","utf-8");
$resbj="=?iso-2022-jp?B?".base64_encode(mb_convert_encoding($resbj,"JIS","utf-8"))."?=";
//$resbj="=?iso-2022-jp?B?".base64_encode(mb_convert_encoding($resbj,"JIS","utf-8"))."?=";

$reheader="From: $to\nReply-To: ".$to."\nContent-Type: text/plain;charset=utf-8\nX-Mailer: PHP/".phpversion();

}

//$body=mb_convert_encoding($body,"JIS","utf-8");
$sbj="=?iso-2022-jp?B?".base64_encode(mb_convert_encoding($sbj,"JIS","utf-8"))."?=";
if($from_add == 1) {
  $from = $_POST['Email'];
  $header="From: $from\nReply-To: ".$_POST['Email']."\nContent-Type:text/plain;charset=utf-8\nX-Mailer: PHP/".phpversion();
} else {
  $header="Reply-To: ".$to."\nContent-Type:text/plain;charset=utf-8\nX-Mailer: PHP/".phpversion();
}
if($chmail == 0 || $sendmail == 1) {
  //putsCSVToFile($csv_row, '/home/morimotown/mori-d.net/public_html/contact/contact.csv');
  mail($to,$sbj,$body,$header);
  if($remail == 1) { mail($reto,$resbj,$rebody,$reheader); }

}
else { htmlHeader();
?>








<!-- 送信確認画面のレイアウト　-->

<p class="sub">入力項目にお間違いないかご確認いただき送信下さい。</p>

<form action="<?php echo $file_name; ?>#mail_link" method="POST">
<?php echo $err_message; ?>
<table>
<?php
foreach($_POST as $key=>$val) {
  $key = strtr($key, $string_from, $string_to);
  
  //※numata追記　チェックボックス（配列）の場合は以下の処理で複数の値を取得するように変更　HTML側のname属性の値にも[と]を追加する。
  $out = '';
  if(is_array($val)){
  foreach($val as $item){ 
  $out .= $item . ''; 
  }
  if(substr($out,strlen($out) - 1,1) == '-') { 
  $out = substr($out, 0 ,strlen($out) - 1); 
  }
 }
    else {
        $out = $val;
}
  //チェックボックス（配列）追記ここまで
  
  if(get_magic_quotes_gpc()) { $out = stripslashes($out); }
  $out = htmlspecialchars($out);
  $key = htmlspecialchars($key);
  print("<tr bgcolor=#ffffff><td class=\"l_Cel\">".$key."</td><td>".$out);
?>
<input type="hidden" name="<?php echo $key; ?>" value="<?php echo $out; ?>">
<?php
  print("</td></tr>\n");
}
?>
</table>
<br>
<div class="center">
<input type="hidden" name="mail_set" value="submit">
<input type="submit" value="送信する" class="btn btn_area btn_txt">　
<input type="button" value="入力画面に戻る" class="btn btn_area btn_txt" onclick="history.back()">
</div>
</form>



<?php htmlFooter(); } if(($jpage == 0 && $sendmail == 1) || ($jpage == 0 && ($chmail == 0 && $sendmail == 0))) { htmlHeader(); ?>



<!-- 送信終了画面のレイアウト-->



<h4><a class="scroll">お問い合わせいただきまして誠にありがとうございます。</a></h4>
<div class="center pb60 allbtn_p mt100">
送信ありがとうございました。<br>
送信は正常に完了しました。<br><br>
<div class="view-more"><a href="<?php echo $site_top; ?>" class="btn"><span>トップページへ戻る⇒</span></a></div>
</div>


<?php htmlFooter(); } else if(($jpage == 1 && $sendmail == 1) || $chmail == 0) { header("Location: ".$next); } function htmlHeader() { ?>
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ja"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="ja"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="ja"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="ja"><!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="OS産業株式会社,天井クレーン,橋形クレーン,特殊クレーン,ホイスト式クレーン,クラブ式クレーン">
<meta name="description" content="兵庫県姫路市のオーエス産業株式会社-荷役搬送機械メーカー（クレーン・コンベアなど）">
<meta name="robots" content="index,follow">
<title>オーエス産業株式会社 ::: 特殊クレーン/天井クレーン/橋形クレーンなど荷役運搬機械メーカー</title>
<link rel="icon" href="/common/img/favicon.ico">
<link rel="stylesheet" href="/common/css/normalize.css">
<link rel="stylesheet" href="/common/css/common.css">
<link rel="stylesheet" href="/contact/css/contact.css">
<style type="text/css">
@media screen and (min-width: 768px) {
	#wrapper{
		margin:0 auto !important;
		color:#666 !important;
	}
	#mail_link{
		margin-top: -90px;
		padding-top: 90px;
	}
	table{
		width: 860px;
		margin:30px auto 0 !important;
		border-collapse:collapse !important;
		font-size: 14px !important;
		box-sizing:border-box;
	}
	table tr{
		background-color: #ffffff !important;
		border-top: 1px gray solid !important;
	}
	table tr:first-child{
		border-top: none;
	}
	td{
		border:1px solid #ccc !important;
		padding:3px !important;
		color:#666 !important;
		padding: 20px !important;
		font-size: 14px;
	}
	td.l_Cel{
		width: 140px !important;
		font-size: 14px;
	}
	div.center{
		margin: 50px auto 0 !important;
		line-height: 1.6;
		text-align: center;
	}
	.confirm{
		margin: 30px auto 0;
		width: 926px;
	}
	.pb60{
		padding-bottom: 60px;
	}
	.end{
		margin-top: 100px;
		font-size: 16px;
	}
	.center input{
		background-color: #1177bf;
		padding: 20px 30px;
		color: #fff;
		font-size: 15px;
		border: none;
		cursor: pointer;
		box-sizing: border-box;
		text-align: center;
		width: 280px !important;
		margin: 0 !important;
	}
	.center input:hover{
		color: #fff;
	}
	h4{
		text-align: center;
		font-size: 20px;
	}
	p.sub {
		margin-top: 40px;
		text-align: center;
		font-size: 120%;
	}
	center.mtauto {
		margin-top: 80px;
	}
}
@media screen and (min-width: 0px) and (max-width: 767px){
	#main_area{
		color:#666 !important;
	}
	#mail_link{
		margin-top: -65px;
		padding-top: 65px;
	}
	p.sub{
		margin-bottom: 20px;
	}
	table{
		width:100%;
		margin:0 auto !important;
		border-collapse:collapse !important;
		font-size: 12px !important;
	}
	table tr{
		background-color: #ffffff !important;
		border-top: 1px gray solid !important;
	}
	table tr:first-child{
		border-top: none;
	}
	td{
		border:1px solid #ccc !important;
		padding:3px !important;
		color:#666 !important;
		padding: 10px !important;
		font-size: 12px;
		display: table-cell !important;
	}
	td.l_Cel{
		width: 100px !important;
		font-size: 12px;
	}
	div.center{
		margin: 0px auto 10px;
		line-height: 1.6;
		text-align: center;
	}
	.pb60{
		padding-bottom: 60px;
	}
	.txt{
		margin-bottom: 15px;
	}
	p.sub {
		margin-top: 40px;
		text-align: center;
	}
	h4{
		text-align: center;
		font-size: 16px;
		padding-bottom: 30px;
	}
	.center input{
		width: 130px;
		padding: 15px 20px;
		box-sizing: border-box;
		margin-top: 0 !important;
		margin-bottom: 0 !important;
		background-color: #1177bf;
		color: #fff;
		font-size: 12px;
		border: none;
		cursor: pointer;
		-webkit-appearance: none;
	}
	.center input:hover{
		color: #fff;
	}
}
</style>
</head>
<body>
<header id="header" class="header">
    <div class="container">
        <h1 class="site-logo">
            <a href="/"><img src="/common/img/logo.png" alt="オーエス産業株式会社"></a>
        </h1>
        <nav class="gnav">
            <ul>
                <li><a href="/advantage/">オーエスの強み<span>ADVANTAGE</span></a></li>
                <li><a href="/product/">製品情報<span>PRODUCT</span></a></li>
                <li><a href="/company/">企業情報<span>COMPANY</span></a></li>
                <li><a href="/contact/">お問い合わせ<span>CONTACT</span></a></li>
                <li><a href="/recruit/">採用情報<span>RECRUIT</span></a></li>
            </ul>
        </nav>
        <div id="hambuger">
            <div class="nav-icon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
</header>
<!-- end header -->
<nav class="gnav_sp">
    <ul>
        <li><a href="/">HOME</a></li>
        <li>
            <a href="/advantage/">オーエスの強み</a>
            <ul>
                <li><a href="/advantage/development.html">開発体制・技術力</a></li>
                <li><a href="/advantage/support.html">サポート・メンテナンス</a></li>
                <li><a href="/advantage/quality.html">品質・安全への取り組み</a></li>
            </ul>
        </li>
        <li>
            <span>製品情報</span>
            <ul>
                <li><a href="/product/">製品一覧</a></li>
                <li><a href="/product/ceiling.html">天井クレーン</a></li>
                <li><a href="/product/bridge.html">橋形クレーン</a></li>
                <li><a href="/product/special.html">特殊クレーン</a></li>
                <li><a href="/product/other.html">その他製品</a></li>
                <li><a href="/product/data01.html">天井クレーン使用詳細</a></li>
                <li><a href="/product/rail.html">走行レール敷設</a></li>
                <li><a href="/product/building.html">建屋内設置寸法</a></li>
                <li><a href="/product/ladder.html">はしご道寸法</a></li>
                <li><a href="/product/runway.html">ウレタン車輪用ランウェイガーダ設置注意事項</a></li>
            </ul>
        </li>
        <li>
            <span>企業情報</span>
            <ul>
                <li><a href="/company/">ごあいさつ</a></li>
                <li><a href="/company/outline.html">会社概要</a></li>
                <li><a href="/company/organization.html">組織図</a></li>
                <li><a href="/company/factory.html">工場設備</a></li>
            </ul>
        </li>
        <li>
            <a href="/contact/">お問い合わせ</a>
            <ul>
                <li><a href="/form_contact/">お問い合わせフォーム</a></li>
                <li><a href="/form_estimate/">お見積りフォーム</a></li>
            </ul>
        </li>
        <li>
            <span>その他</span>
            <ul>
                <li><a href="/recruit/">採用情報</a></li>
                <li><a href="/privacy/">個人情報保護方針</a></li>
                <li><a href="/sitemap/">サイトマップ</a></li>
            </ul>
        </li>
    </ul>
    <div class="nav-close">
        <span>閉じる</span>
    </div>
</nav>
<div class="page-title">
    <h1 class="page-title_ttl">お問い合わせ</h1>
</div>
<div class="breadcrumb">
    <div class="container">
        <ul>
            <li><a href="/">トップページ</a></li>
            <li><a href="/contact/">お問い合わせ</a></li>
            <li>お問い合わせフォーム</li>
        </ul>
    </div>
</div>
<main class="main">
    <div class="b_contact">
        <div class="container">
            <div class="b_form">
                <h2 class="ttl_art">お問い合わせフォーム</h2>
                <div class="ctn_f" id="mail_link">
		
		
<?php } function htmlFooter() { ?>

                </div>
                <!--/.ctn_f-->
            </div>
            <!--/.b_form-->
        </div>
    </div>
    <!--/.b_contact-->
</main>
<!-- end main -->
<footer id="footer" class="footer">
    <div class="container">
        <div class="footer-inner">
            <div class="footer-info">
                <h3>オーエス産業株式会社</h3>
                <p>〒670-0804　姫路市保城津倉188<br>
                TEL ： 079-281-1530<br>
                FAX ： 079-288-0928</p>
            </div>
            <div class="footer-nav">
                <ul>
                    <li><a href="/">HOME</a></li>
                </ul>
                <ul>
                    <li><a href="/advantage/">オーエスの強み</a></li>
                    <li><a href="/advantage/development.html">開発体制・技術力</a></li>
                    <li><a href="/advantage/support.html">サポート・メンテナンス</a></li>
                    <li><a href="/advantage/quality.html">品質・安全への取り組み</a></li>
                </ul>
                <ul>
                    <li><a href="/company/">ごあいさつ</a></li>
                    <li><a href="/company/outline.html">会社概要</a></li>
                    <li><a href="/company/organization.html">組織図</a></li>
                    <li><a href="/company/factory.html">工場設備</a></li>
                </ul>
                <ul>
                    <li><a href="/product/">製品一覧</a></li>
                    <li><a href="/product/ceiling.html">天井クレーン</a></li>
                    <li><a href="/product/bridge.html">橋形クレーン</a></li>
                    <li><a href="/product/special.html">特殊クレーン</a></li>
                    <li><a href="/product/other.html">その他製品</a></li>
                </ul>
                <ul>
                    <li><a href="/product/data01.html">天井クレーン仕様詳細</a></li>
                    <li><a href="/product/rail.html">走行レール敷設</a></li>
                    <li><a href="/product/building.html">建屋内設置寸法</a></li>
                    <li><a href="/product/ladder.html">はしご道寸法</a></li>
                    <li><a href="/product/runway.html">ウレタン車輪用ランウェイ<br>ガーダ設置注意事項</a></li>
                </ul>
                <ul>
                    <li><a href="/contact/">お問い合わせ</a></li>
                    <li><a href="/form_contact/">お問い合わせフォーム</a></li>
                    <li><a href="/form_estimate/">お見積りフォーム</a></li>
                </ul>
                <ul>
                    <li><a href="/recruit/">採用情報</a></li>
                    <li><a href="/privacy/">個人情報保護方針</a></li>
                    <li><a href="/sitemap/">サイトマップ</a></li>
                </ul>
            </div>
        </div>
        <small class="copyright">Copyright &copy; OS SANGYO Co., Ltd. All Rights Reserve</small>
    </div>
</footer>
<!-- end footer -->
<script src="/common/js/jquery-1.11.3.min.js"></script>
<script src="/common/js/base.js"></script>
<script src="/common/js/main.js"></script>
<script src="/contact/js/ajaxzip3.js"></script>
<script src="/contact/js/form.js"></script>
<script>
    jQuery('#send').click(function(event) {
        setTimeout(function(){
            if(jQuery('.frm_group .left.error').length > 0 || jQuery('.frm_group .b_input.error').length > 0){
                jQuery('.head-error').show();
            }else{
                jQuery('.head-error').hide();
            }
        }, 500);       
    });
</script>
</body>

</html>

<?php } ?>


