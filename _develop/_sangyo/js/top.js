$(document).ready(function() {
	$('.main-slider').slick({
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 1000,
        fade: true,
        cssEase: 'ease-in-out'
    });
});

$(window).on('load',function(){
	$('.main-slider_item--txt').addClass('bounceInLeft');
});