$( document ).ready(function() {
  	$("#hambuger").click(function() {
  	    $(this).find('.nav-icon').toggleClass("open");
  	    $(".gnav_sp").toggleClass("opened");
          $(".header").toggleClass("bg");
          $("body").toggleClass("bg");
  	});
    $(".nav-close span").click(function() {
        $('.nav-icon').removeClass("open");
        $(".gnav_sp").removeClass("opened");
        $(".header").removeClass("bg");
        $("body").removeClass("bg");
    });

    if($('#header').length > 0 ){
      var box    = $("#header");
        var boxTop = box.offset().top;
        $(window).scroll(function () {
            if($(window).scrollTop() > 0) {
                box.addClass("fixed");
            } else {
                box.removeClass("fixed");
            }
        });
    }
});
$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
});

// Fix IE
var addClassltIE11 = function() {
  if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
    if (window.navigator.userAgent.indexOf("Windows NT 10.0") != -1 || window.navigator.userAgent.indexOf("Windows NT 6.3") != -1) {
      $('body').addClass('ltie11');
    } else if (window.navigator.userAgent.indexOf("Windows NT 6.1") != -1) {
      $('body').addClass('ltie7');
    }
  }
}

$(function() {
  addClassltIE11();
});

jQuery(function($) {
    $('.btn01 a').click(function(){
      $('.wrp01').show();
      return false;
    });
    $('.btn02 a').click(function(){
      $('.wrp02').show();
      return false;
    });
    $('.btn03 a').click(function(){
      $('.wrp03').show();
      return false;
    });
    $('.btn04 a').click(function(){
      $('.wrp04').show();
      return false;
    });
    $('.close').click(function(){
      $('.wrap-video').hide();
    });
    $('.wrap-video').click(function(){
      $(this).hide();
    });
});

$(document).ready(function(){
  $(document).click('close', function() {
    $('iframe').each(function(){
    var url = $(this).attr("src")
    $(this).attr("src",url+"?wmode=transparent")
    });
  });
});